package fr.uavignon.ceri.tp2;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

@Dao
public interface BookDao {

    //insertBook(Book book) pour insérer un nouveau livre dans la base de données
    @Insert
    void insertBook(Book book);

    //getBook(long id) pour obtenir un livre suivant son identifiant
    @Query("SELECT * FROM books WHERE bookId = :id")
    List<Book> getBook(long id);

    //deleteBook(long id) pour supprimer un livre
    @Query("DELETE FROM books WHERE bookId = :id")
    void deleteBook(long id);

    //updateBook(Book book) pour mettre à jour les informations d’un livre existant déjà dans la base de données
    @Update
    void updateBook(Book book);

    //getAllBooks() retournant la liste de toutes les livres contenus dans la base
    @Query("SELECT * FROM books")
    LiveData<List<Book>> getAllBooks();

    @Query("DELETE FROM books")
    void deleteAllBooks();

}
