package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private DetailViewModel viewModel;
    private EditText title;

    private EditText authors;

    private EditText year;

    private EditText genres;

    private EditText publisher;

    private Button updateBook;

    public Book bookUpdate;

    private boolean exist = true;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        viewModel.setSearchBook((int)args.getBookNum());
        if((int)args.getBookNum() == -1){
            exist = false;
        }



        title = (EditText) view.findViewById(R.id.nameBook);
        authors = (EditText) view.findViewById(R.id.editAuthors);
        year = (EditText) view.findViewById(R.id.editYear);
        genres = (EditText) view.findViewById(R.id.editGenres);
        publisher = (EditText) view.findViewById(R.id.editPublisher);

        updateBook = (Button) view.findViewById(R.id.buttonUpdate);


        listenerSetup();
        //observerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);

            }
        });
    }



/*
    private void observerSetup() {
        viewModel.getSearchBook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(Book book) {
                        bookUpdate = book;

                        if(book != null) {
                            title.setText(book.getTitle());

                            authors.setText(book.getAuthors());

                            year.setText(book.getYear());

                            genres.setText(book.getGenres());

                            publisher.setText(book.getPublisher());
                        }
                    }
                });
    }

    */

    private void listenerSetup() {

        updateBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(exist == true){
                    String title = DetailFragment.this.title.getText().toString();
                    String auteur = authors.getText().toString();
                    String year = DetailFragment.this.year.getText().toString();
                    String genre = genres.getText().toString();
                    String editeur = publisher.getText().toString();

                    DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
                    int id = (int)args.getBookNum();

                    Book newBook = new Book(title, auteur, year, genre, editeur);
                    newBook.setId(id);
                    viewModel.insertOrUpdateBook(newBook);
                }
                else{
                    if(TextUtils.isEmpty(title.getText().toString()) || TextUtils.isEmpty(authors.getText().toString())){
                        Snackbar.make(view, "Titre et auteur manquant", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    else{
                        Snackbar.make(view, "Champs manquants", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        String title = DetailFragment.this.title.getText().toString();
                        String auteur = authors.getText().toString();
                        String year = DetailFragment.this.year.getText().toString();
                        String genre = genres.getText().toString();
                        String editeur = publisher.getText().toString();

                        //DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
                        //int id = (int)args.getBookNum();

                        Book newBook = new Book(title, auteur, year, genre, editeur);
                        newBook.setId(-1);
                        viewModel.insertOrUpdateBook(newBook);
                        Snackbar.make(view, title+"Livre ajouté ", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }








            }
        });

    }




}
